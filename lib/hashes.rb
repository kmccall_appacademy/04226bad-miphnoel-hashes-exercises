# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  str.split.reduce({}) do |hash, word|
    hash[word] = word.length
    hash
  end
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |item, quantity|
    older[item] = quantity
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.

def counter_hash(arr)
  counts = Hash.new(0)

  arr.each { |el| counts[el] += 1 }

  counts
end

def letter_counts(word)
  counter_hash(word.chars)
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  arr.map { |el| [el, 0] }.to_h.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  freq_of_even = numbers.count(&:even?)
  [[:even, freq_of_even], [:odd, (numbers.size - freq_of_even)]].to_h
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = string.chars.select { |ch| "aeiou".include?(ch) }
  vowel_counts = counter_hash(vowels)

#  vowel_counts = Hash.new(0)
#  string.chars.each do |ch|
#    vowel_counts[ch] += 1 if "aeiou".include?(ch)
#  end

  vowel_counts.to_a.sort_by { |vowel| [-vowel[1], vowel[0]] }.first[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  late_yr_students = students.select do |student, birthday|
    birthday > 6
  end

  combinations(late_yr_students.keys)
end

def combinations(array)
  combinations = []

  array.each_with_index do |el, idx|
    i = 0
    while idx + i < array.size - 1
      i += 1
      combinations.push([el, array[idx + i]])
    end
  end

  combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species_counts = counter_hash(specimens)
  number_of_species = species_counts.length
  smallest_population = species_counts.values.min
  largest_population = species_counts.values.max

  number_of_species**2 * smallest_population / largest_population
end

# def species_counter(specimens)
#   counts = Hash.new(0)
#
#   specimens.each { |specimen| counts[specimen] += 1 }
#
#   counts
# end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  letters_available = sign_letter_counts(normal_sign)
  letters_required = sign_letter_counts(vandalized_sign)

  letters_required.all? do |ch, count|
    count <= letters_available[ch]
  end
end

def downcase_punctuationless_arr(str)
  str.downcase.delete(".,?!'-_:;").chars
end

def sign_letter_counts(str)
  counter_hash(downcase_punctuationless_arr(str))
end
# def character_count(str)
#   counts = Hash.new(0)
#
#   str.chars.each { |ch| counts[ch] += 1 }
#
#   counts
# end
